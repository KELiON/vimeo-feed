import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import getTop from './lib/vimeo/getTop'
import './index.css'

ReactDOM.render(
  <App videos={getTop()} />,
  document.getElementById('root')
)
