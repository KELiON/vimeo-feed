import topJson from './top'
import formatVideo from './formatVideo'

/**
 * Get top videos from vimeo
 *
 * @return {Array}
 */
export default () => {
  return topJson.data.map(formatVideo)
}
