/**
 * Default user avatar object
 * @type {Object}
 */
const DEAFULT_AVATAR = {
  width: 100,
  height: 100,
  link: 'https://i.vimeocdn.com/portrait/240766_300x300'
}

/**
 * Get avatar for user
 *
 * @param  {Object} user
 * @return {Object}
 */
const getAvatar = (user) => {
  const pictures = user.pictures || {}
  const sizes = pictures.sizes || []
  return sizes.find(({width}) => width === 300) || DEAFULT_AVATAR
}


/**
 * Format user object
 *
 * @param  {Object} user
 * @return {Object}
 */
export default (user) => {
  const { link, name } = user
  const likes = user.metadata.connections.likes.total
  return {
    link,
    name,
    likes,
    avatar: getAvatar(user)
  }
}
