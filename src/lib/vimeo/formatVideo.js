import formatUser from './formatUser'

/**
 * Format video object.
 *
 * @param  {Object} video Video object from vimeo API response
 * @return {Object}
 */
export default (video) => {
  const { uri, name, description, link, metadata, user } = video
  return {
    // Main video information
    uri,
    name,
    description,
    link,

    // Metadata
    plays: video.stats.plays,
    likes: metadata.connections.likes.total,
    comments: metadata.connections.comments.total,

    // Formatted user
    user: formatUser(user)
  }
}
