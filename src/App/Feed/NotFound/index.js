import React from 'react'
import './styles.css'

/**
 * Component to show that nothing to show in the feed
 *
 * @return {React.Component}
 */
export default () => {
  return (
    <div className='not-found'>
      No videos found
    </div>
  )
}
