import React, { PropTypes } from 'react'
import './styles.css'

/**
 * Video component. Item in videos feed
 *
 * @param  {Object} props.video
 * @return {React.Component}
 */
const Item = ({video}) => {
  const { user } = video
  return (
    <div className='video'>
      <a href={user.link} className='avatar-wrapper'>
        <img
          src={user.avatar.link}
          alt={user.name}
        />
      </a>
      <div className='video-details'>
        <div className='video-header'>
          <a href={video.link} className='video-name'>{video.name}</a>
          <div className='video-by'>
            by <a className='video-username' href={user.link}>{user.name}</a>
          </div>
        </div>
        <div className='video-description'>
          { video.description }
        </div>
        <div className='video-meta-info'>
          { video.plays && <div className='video-meta video-meta__plays'>{video.plays}</div> }
          { video.likes && <div className='video-meta video-meta__likes'>{video.likes}</div> }
          { video.comments && <div className='video-meta video-meta__comments'>{video.comments}</div> }
        </div>
      </div>
    </div>
  )
}

Item.propTypes = {
  video: PropTypes.shape({
    link: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    plays: PropTypes.number,
    likes: PropTypes.number,
    comments: PropTypes.number,
    user: PropTypes.shape({
      link: PropTypes.string,
      name: PropTypes.string,
      avatar: PropTypes.shape({
        link: PropTypes.string
      })
    })
  })
}

export default Item
