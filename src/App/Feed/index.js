import React, { Component, PropTypes } from 'react'
import Item from './Item'
import NotFound from './NotFound'
import './styles.css'

export default class Feed extends Component {
  static propTypes = {
    videos: PropTypes.array.isRequired,
    perPage: PropTypes.number.isRequired,
    onShowMore: PropTypes.func.isRequired,
  }
  /**
   * Render Show More button in the bottom of feed
   *
   * @param  {Array} feed Feed that is currently displayed
   * @return {React.Component}
   */
  renderMoreButton(feed) {
    const { videos, perPage, onShowMore } = this.props
    const moreVideos = Math.min(videos.length - feed.length, perPage)
    return (
      <button onClick={onShowMore} className='feed-show-more'>Show {moreVideos} more</button>
    )
  }

  render() {
    const { videos, perPage, currentPage } = this.props
    const feed = videos.slice(0, perPage * (currentPage + 1))
    if (feed.length === 0) {
      return <NotFound />
    }
    return (
      <div>
        <div className='feed'>
          { feed.map(video => <Item key={video.uri} video={video} /> ) }
        </div>
        { feed.length < videos.length && this.renderMoreButton(feed) }
      </div>
    )
  }
}
