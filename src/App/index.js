import React, { Component, PropTypes } from 'react'
import Feed from './Feed'
import Filter from './Filter'
import './styles.css'

/**
 * Get filter by text function
 *
 * @param  {String} text
 * @return {Function}
 */
const filterByText = (text) => {
  const searchText = text.toLowerCase()
  return ({description}) => (
    description && description.toLowerCase().indexOf(searchText) !== -1
  )
}

/**
 * Filter videos by user likes count
 *
 * @param  {Object} video
 * @return {Boolean}
 */
const filterByLikes = (video) => video.user.likes >= 10

export default class App extends Component {
  static propTypes = {
    videos: PropTypes.array.isRequired
  }
  constructor(props) {
    super(props)

    // Set initial state
    this.state = {
      perPage: 10,
      currentPage: 0,
      onlyLikedUsers: false,
      textFilter: ''
    }

    // Bind event handlers
    this.onShowMore = this.onShowMore.bind(this)
    this.onPerPageChanged = this.onPerPageChanged.bind(this)
    this.onUsersWithLikesChanged = this.onUsersWithLikesChanged.bind(this)
    this.onTextFilterChanged = this.onTextFilterChanged.bind(this)
  }

  /**
   * Apply user filters for props.videos
   *
   * @return {Array}
   */
  filterVideos() {
    const { onlyLikedUsers, textFilter} = this.state
    const filterByDescription = filterByText(textFilter)
    return this.props.videos.filter(video => {
      // By default show all videos
      let result = true

      // Apply likes filter if it is selected
      result = result && (!onlyLikedUsers || filterByLikes(video))

      // Apply text filter if something is entered
      result = result && (!textFilter || filterByDescription(video))
      return result
    })
  }

  /**
   * Handle click on Show More button
   *
   */
  onShowMore() {
    this.setState({
      currentPage: this.state.currentPage + 1
    })
  }

  /**
   * Handle "Only liked users" checkbox change
   *
   * @param  {Event} event
   */
  onUsersWithLikesChanged(event) {
    this.setState({
      onlyLikedUsers: event.target.checked,
      currentPage: 0
    })
  }

  /**
   * Handle changes in search in description filter
   *
   * @param  {Event} event
   */
  onTextFilterChanged(event) {
    this.setState({
      textFilter: event.target.value,
      currentPage: 0
    })
  }

  /**
   * Handle changes of per page select
   *
   * @param  {Event} event
   */
  onPerPageChanged(event) {
    this.setState({
      perPage: parseInt(event.target.value, 10),
      currentPage: 0
    })
  }

  render() {
    const videos = this.filterVideos()
    const { perPage, currentPage, onlyLikedUsers, textFilter } = this.state
    return (
      <div className='app'>
        <Filter
          perPage={perPage}
          onPerPageChanged={this.onPerPageChanged}
          onlyLikedUsers={onlyLikedUsers}
          onUsersWithLikesChanged={this.onUsersWithLikesChanged}
          textFilter={textFilter}
          onTextFilterChanged={this.onTextFilterChanged}
        />
        <Feed
          videos={videos}
          perPage={perPage}
          currentPage={currentPage}
          onShowMore={this.onShowMore}
        />
      </div>
    )
  }
}
