import React, { PropTypes } from 'react'
import './styles.css'

/**
 * Filter component.
 * Includes text filter, likes filter and per page selector
 *
 * @param  {Object} props
 * @return {React.Component}
 */
const Filter = (props) => {
  const {
    perPage, onPerPageChanged,
    onlyLikedUsers, onUsersWithLikesChanged,
    textFilter, onTextFilterChanged
  } = props
  return (
    <div className='filter'>
      <div className='filter-field'>
        <label htmlFor='liked-term' className='filter-label'>Search in description:</label>
        <input type='search' value={textFilter} id='filter-term' onChange={onTextFilterChanged} />
      </div>

      <div className='filter-field'>
        <label htmlFor='per-page' className='filter-label'>Per page:</label>
        <select value={perPage} onChange={onPerPageChanged} id='per-page'>
          <option value={10}>10</option>
          <option value={25}>25</option>
          <option value={50}>50</option>
        </select>
      </div>
      <div className='filter-field'>
        <input type='checkbox' checked={onlyLikedUsers} onChange={onUsersWithLikesChanged} id='liked-users' />
        <label htmlFor='liked-users' className='filter-label'>Only users with 10+ likes</label>
      </div>
    </div>
  )
}

Filter.propTypes = {
  perPage: PropTypes.number.isRequired,
  onlyLikedUsers: PropTypes.bool.isRequired,
  textFilter: PropTypes.string.isRequired,
  onPerPageChanged: PropTypes.func.isRequired,
  onUsersWithLikesChanged: PropTypes.func.isRequired,
  onTextFilterChanged: PropTypes.func.isRequired,
}

export default Filter
